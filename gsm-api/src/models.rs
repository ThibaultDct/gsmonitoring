use diesel;
use diesel::prelude::*;
use diesel::sql_types::Uuid;
use diesel::pg::PgConnection;

#[derive(Queryable)]
pub struct Server {
    pub id: Uuid,
    pub name: String,
    pub server_ip: String,
}